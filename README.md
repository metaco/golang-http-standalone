HTTP STANDALONE

# 表格驱动测试

- 可以部分失败

- 测试数据和测试可以分离

## 测试 command line 

- 进入测试代码目录

```bash
cd pkg/model

go test .
```

- 测试代码覆盖率

```bash
go test -coverprofile=c.out
```

- 查看代码覆盖率文本

```bash
go tool cover -html=c.out
```


- 测试性能

```go
cd pkg/model
go test -bench .
```