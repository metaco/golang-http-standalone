package main

import (
	"flag"
	"fmt"
	"httpstandalone/pkg/endpoint"
	"httpstandalone/pkg/middleware"
	"httpstandalone/pkg/model"
	"httpstandalone/pkg/tasksched"
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

func init() {
	endpoint.ParseRunCommand()
}

func main() {
	flag.Parse()
	// init ZapLogger
	logger := middleware.AppLogger{}
	err := logger.InitLogger(endpoint.ServerArgument.AppLogPath)
	if err != nil {
		log.Fatal(err)
	}

	initDbError := model.Ent.InitDataBaseConnection(endpoint.ServerArgument.DatabaseConfig)
	if initDbError != nil {
		log.Fatal(initDbError)
	}

	tasksched.ReqTicker.InitRequestCounter(logger.Logger)
	// gin.SetMode(gin.DebugMode)
	// gin.DisableConsoleColor()
	engine := gin.Default()
	// engine.Use(logger.LoggerEndpointDuration)

	engine.GET("/project/:id", endpoint.GetProject)
	engine.POST("/project", endpoint.PostProject)
	engine.PUT("/project/:id", endpoint.PutProject)
	engine.DELETE("/project/:id", endpoint.DeleteProject)

	serverPort := os.Getenv("SERVER_PORT")
	if serverPort == "" && endpoint.ServerArgument.Port == "" {
		serverPort = "8088"
	}
	if len(endpoint.ServerArgument.Port) != 0 {
		serverPort = endpoint.ServerArgument.Port
	}
	log.Fatal(engine.Run(fmt.Sprintf(":%s", serverPort)))
}
