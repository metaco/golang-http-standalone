package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/dialect"
	"github.com/facebook/ent/dialect/entsql"
	"github.com/facebook/ent/schema"
	"github.com/facebook/ent/schema/field"
	"time"
)

// ProjectBase holds the schema definition for the ProjectBase entity.
type ProjectBase struct {
	ent.Schema
}

// Fields of the ProjectBase.
func (ProjectBase) Fields() []ent.Field {
	return []ent.Field{
		field.Int64("id").Unique().Immutable(),
		field.String("name"),
		field.String("property").Default(""),
		field.String("description").Default(""),
		field.Enum("status").Values("enable", "disable").Default("enable"),
		field.Float("base_quota").SchemaType(map[string]string{
			dialect.MySQL : "decimal(10, 2)",

		}),

		field.Time("created_at").SchemaType(map[string]string{ dialect.MySQL: "datetime" }).Optional().
			Default(time.Now).Immutable(),
		field.Time("updated_at").SchemaType(map[string]string{ dialect.MySQL: "datetime" }).Optional().
			Default(time.Now),
	}
}

// Edges of the ProjectBase.
func (ProjectBase) Edges() []ent.Edge {
	return nil
}

func (ProjectBase) Annotations() []schema.Annotation {
	return []schema.Annotation {
		entsql.Annotation{
			Table:       "project_base",
		},
	}
}
