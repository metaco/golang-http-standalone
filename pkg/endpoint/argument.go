package endpoint

import "flag"

// HTTPServerArgument 服务运行参数
type HTTPServerArgument struct {
	Port           string
	DatabaseConfig string
	AppLogPath     string
}

// ServerArgument 运行参数
var ServerArgument HTTPServerArgument

// ParseRunCommand 解析命令行参数
func ParseRunCommand() {
	flag.StringVar(&ServerArgument.Port, "port", "", "http server started port (.eg.8088)")
	flag.StringVar(&ServerArgument.DatabaseConfig, "db-config", "/tmp/db.json", "database config path (.eg. /tmp/db.json)")
	flag.StringVar(&ServerArgument.AppLogPath, "log-path", "/tmp/app.log", "app log path (.eg. /tmp/app.log)")
}
