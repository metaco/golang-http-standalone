package endpoint

import (
	"fmt"
	"httpstandalone/ent"
	"httpstandalone/pkg/model"
	"httpstandalone/pkg/tasksched"

	"github.com/gin-gonic/gin"
)

// ProjectInfo Req 请求参数
type ProjectInfo struct {
	ProjectID      int64  `uri:"id" binding:"required"` // binding 必须要加
	ProjectName    string `form:"projectName"`
	ProjectEndTime string `form:"projectEndTime"`
}

// GetProject 资源端点
func GetProject(context *gin.Context) {
	projectInfo := new(ProjectInfo)
	// var projectId string
	if err := context.ShouldBindUri(&projectInfo); err != nil {
		r := JSONResult{
			Message: "invalid path project Id parameter",
		}
		r.InvalidResponse(context)
		return
	}
	projectBase, err := model.Ent.FindProjectBaseById(projectInfo.ProjectID)
	if err != nil {
		r := JSONResult{
			Message: fmt.Sprintf("%s by projectbase Id %d", err.Error(), projectInfo.ProjectID),
		}
		r.NotFoundResponse(context)
		return
	}
	r := JSONResult{
		Data: projectBase,
	}
	// 发送一条请求完毕的 channel message
	tasksched.ReqTicker.ReqCount <- true
	r.OkResponse(context)
}

// PostProject post
func PostProject(context *gin.Context) {
	projectInfo := new(ProjectInfo)
	if err := context.Bind(&projectInfo); err != nil {
		r := JSONResult{
			Message: "invalid parameters",
		}
		r.InvalidResponse(context)
		return
	}

	if len(projectInfo.ProjectName) == 0 {
		r := JSONResult{
			Message: "invalid parameters ProjectName",
		}
		r.InvalidResponse(context)
		return
	}

	projectBase := ent.ProjectBase{
		Name:        projectInfo.ProjectName,
		Description: "default Description",
		Status:      "disable",
		Property:    "havingGR",
		BaseQuota:   230.12,
	}

	saveProjectBase, err := model.Ent.SaveProjectBase(projectBase)
	if err != nil {
		r := JSONResult{
			Message: err.Error(),
		}
		r.InternalErrorResponse(context)
		return
	}
	r := JSONResult{Data: saveProjectBase}
	r.OkResponse(context)
}

// PutProject Put Project 参数绑定，Path Uri, RequestBody
func PutProject(context *gin.Context) {
	projectBase := new(ProjectInfo)
	// var projectId string
	if err := context.ShouldBindUri(&projectBase); err != nil {
		r := JSONResult{
			Message: "invalid path parameter",
		}
		r.InvalidResponse(context)
		return
	}

	if err := context.Bind(&projectBase); err != nil {
		r := JSONResult{Message: "invalid project parameter"}
		r.InvalidResponse(context)
		return
	}

	b := ent.ProjectBase{
		ID:        projectBase.ProjectID,
		Name:      projectBase.ProjectName,
		Property:  projectBase.ProjectEndTime,
		Status:    "enable",
		BaseQuota: 34536.34,
	}

	projectBaseUpdate, updateError := model.Ent.UpdateProjectBaseById(b)
	if updateError != nil {
		r := JSONResult{Message: updateError.Error()}
		r.InternalErrorResponse(context)
		return
	}
	r := JSONResult{Data: projectBaseUpdate}
	r.OkResponse(context)
}

// DeleteProject 删除ProjectBase
func DeleteProject(context *gin.Context) {
	projectInfo := new(ProjectInfo)
	if err := context.ShouldBindUri(&projectInfo); err != nil {
		r := JSONResult{
			Message: "invalid ProjectBase Id",
		}
		r.InvalidResponse(context)
		return
	}

	deleteRow, err := model.Ent.DeleteProjectBaseByID(projectInfo.ProjectID)
	if err != nil {
		r := JSONResult{
			Message: err.Error(),
		}
		r.InternalErrorResponse(context)
	}

	r := JSONResult{Data: deleteRow}
	r.OkResponse(context)
}
