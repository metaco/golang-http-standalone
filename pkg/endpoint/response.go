package endpoint

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// JSONResult Http 统一响应结构
type JSONResult struct {
	Code    int         `json:"error_code"`
	Message string      `json:"error_message"`
	Data    interface{} `json:"data,omitempty"` // 忽略空值
}

var (
	// InvalidParameterCode 不合法响应状态码
	InvalidParameterCode = -10001
	// ResourceNotFoundCode 资源未找到响应状态码
	ResourceNotFoundCode = -10002
	// ResourceOperationFailedCode 资源操作失败状态码
	ResourceOperationFailedCode = -10010
)

// InvalidResponse 不合法的参数请求响应
func (r *JSONResult) InvalidResponse(ctx *gin.Context) {
	ctx.JSON(http.StatusBadRequest, JSONResult{
		Code:    InvalidParameterCode,
		Message: r.Message,
	})
}

// NotFoundResponse 资源未找到响应
func (r *JSONResult) NotFoundResponse(ctx *gin.Context) {
	ctx.JSON(http.StatusNotFound, JSONResult{
		Code:    ResourceNotFoundCode,
		Message: r.Message,
	})
}

// InternalErrorResponse 内部错误响应
func (r *JSONResult) InternalErrorResponse(ctx *gin.Context) {
	ctx.JSON(http.StatusInternalServerError, JSONResult{
		Code:    ResourceOperationFailedCode,
		Message: r.Message,
	})
}

// OkResponse ok响应
func (r *JSONResult) OkResponse(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, JSONResult{
		Code:    0,
		Message: "ok",
		Data:    r.Data,
	})
}
