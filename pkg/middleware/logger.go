package middleware

import (
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// AppLogger 系统运行日志组件
type AppLogger struct {
	Logger *zap.Logger
}

// InitLogger 日志初始化
func (l *AppLogger) InitLogger(outputPath string) error {
	if l.Logger != nil {
		return nil
	}
	var err error
	encoderConfig := zapcore.EncoderConfig{
		MessageKey:   "message",
		LevelKey:     "level",
		EncodeLevel:  zapcore.CapitalLevelEncoder,
		TimeKey:      "time",
		EncodeTime:   zapcore.ISO8601TimeEncoder,
		CallerKey:    "caller",
		EncodeCaller: zapcore.ShortCallerEncoder,
	}

	l.Logger, err = zap.Config{
		Level:            zap.NewAtomicLevelAt(zapcore.DebugLevel),
		Encoding:         "json",
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig:    encoderConfig,
		OutputPaths:      []string{outputPath},
	}.Build()

	if err != nil {
		return err
	}
	return nil
}

// LoggerEndpointDuration 日志中间件
func (l *AppLogger) LoggerEndpointDuration(context *gin.Context) {
	start := time.Now()

	context.Next()
	l.Logger.Info("incoming request",
		zap.String("path", context.Request.URL.Path),
		zap.Int("HttpStatus", context.Writer.Status()),
		zap.Duration("elapsed", time.Now().Sub(start)),
	)
}
