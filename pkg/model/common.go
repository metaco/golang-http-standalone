package model

import (
	"encoding/json"
	"errors"
	"fmt"
	"httpstandalone/ent"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

// DatabaseType
const (
	DatabaseType     = "mysql"
	DatabaseProtocol = "tcp"
	// DatabaseConfigFilePath = "config/db.json"
	// username:password@protocol(host:port)/dbname
	DatabaseSourceNameTemplate = "%s:%s@%s(%s:%d)/%s?parseTime=true&loc=Local"
)

// EntDataBase 客户端结构
type EntDataBase struct {
	Client *ent.Client
}

// Ent Ent客户端配置
var Ent EntDataBase = EntDataBase{}

// DatabaseConfiguration 数据库配置
type DatabaseConfiguration struct {
	DbUser                  string
	DbPass                  string
	DbHost                  string
	DbPort                  int
	DbName                  string
	DebugMode               bool
	ConnectionMaxLifeMinute time.Duration
	MaxOpenConnection       int
	MaxIdleConnection       int
}

// InitDataBaseConnection 初始化数据库
func (e *EntDataBase) InitDataBaseConnection(dbPath string) error {
	dbJSONFile, err := os.Open(dbPath)
	if err != nil {
		return err
	}
	defer dbJSONFile.Close()

	decoder := json.NewDecoder(dbJSONFile)
	configuration := DatabaseConfiguration{}
	if err = decoder.Decode(&configuration); err != nil {
		return err
	}

	var openError error
	databaseDsn := fmt.Sprintf(DatabaseSourceNameTemplate, configuration.DbUser, configuration.DbPass,
		DatabaseProtocol,
		configuration.DbHost, configuration.DbPort, configuration.DbName,
	)
	e.Client, openError = ent.Open(DatabaseType, databaseDsn)
	if openError != nil {
		return errors.New(openError.Error())
	}
	// 开启ent Debug 日志
	if configuration.DebugMode {
		e.Client = e.Client.Debug()
	}
	return nil
}
