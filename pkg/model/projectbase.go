package model

import (
	"context"
	"httpstandalone/ent"
	"httpstandalone/ent/projectbase"
)

func (e *EntDataBase) FindProjectBaseById(id int64) (ent.ProjectBase, error) {
	projectBase, err := e.Client.ProjectBase.Query().
		Where(projectbase.ID(id)).
		First(context.Background())
	if err != nil {
		return ent.ProjectBase{}, err
	}
	return *projectBase, nil
}

func (e *EntDataBase) SaveProjectBase(project ent.ProjectBase) (int64, error) {
	projectBase, err := e.Client.ProjectBase.
		Create().
		SetName(project.Name).
		SetDescription(project.Description).
		SetBaseQuota(project.BaseQuota).
		SetProperty(project.Property).
		SetStatus(project.Status).Save(context.Background())

	if err != nil {
		return 0, err
	}

	return projectBase.ID, nil
}

func (e *EntDataBase) UpdateProjectBaseById(project ent.ProjectBase) (int, error) {
	savedRows, err := e.Client.ProjectBase.Update().
		SetStatus(project.Status).
		SetName(project.Name).
		SetBaseQuota(project.BaseQuota).
		Where(projectbase.ID(project.ID)).Save(context.Background())
	if err != nil {
		return 0, err
	}

	return savedRows, nil
}

// DeleteProjectBaseByID 删除ProjectBaseId
func (e *EntDataBase) DeleteProjectBaseByID(projectBaseID int64) (int, error) {
	deleteRow, err := e.Client.ProjectBase.Delete().
		Where(projectbase.ID(projectBaseID)).
		Exec(context.Background())
	if err != nil {
		return 0, err
	}
	return deleteRow, nil
}
