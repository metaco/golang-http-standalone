package model

import (
	"log"
	"testing"
)

func TestFindProjectBase(t *testing.T) {
	testData := []struct {
		ID              int64
		ProjectBaseName string
	}{
		{43, "baseNamePackage"},
		{49, "prVr-jectName"},
		{50, "EntsgGOlangF"},
	}
	err := Ent.InitDataBaseConnection("/tmp/db.json")
	if err != nil {
		log.Fatal(err)
	}

	for _, item := range testData {
		projectBase, err := Ent.FindProjectBaseById(item.ID)
		if err != nil {
			t.Fatalf("findById:%d, error: %s", item.ID, err.Error())
		}
		if actual := projectBase.Name; actual != item.ProjectBaseName {
			t.Fatalf("Testing ID：%d, got name: %s expected: %s", item.ID, actual, item.ProjectBaseName)
		}
	}
}

func BenchmarkFindProjectBase(b *testing.B) {
	err := Ent.InitDataBaseConnection("/tmp/db.json")
	if err != nil {
		log.Fatal(err)
	}

	var projectBaseID int64 = 49
	projectBaseName := "prVr-jectName"

	for i := 0; i < b.N; i++ {
		projectBase, err := Ent.FindProjectBaseById(projectBaseID)
		if err != nil {
			b.Errorf("benckmark error:%s", err)
		}
		if actual := projectBase.Name; actual != projectBaseName {
			b.Errorf("got name for %s expected %s", actual, projectBaseName)
		}
	}
}
