package tasksched

import (
	"sync/atomic"
	"time"

	"go.uber.org/zap"
)

// RequestCountTicker 请求技术统计器
type RequestCountTicker struct {
	TimeTicker *time.Ticker
	ReqCount   chan bool
}

// ReqCounter RequestCounter
var ReqCounter int32

// ReqTicker RequestCountTicker
var ReqTicker RequestCountTicker = RequestCountTicker{}

// InitRequestCounter 初始化一个请求计数器
func (r *RequestCountTicker) InitRequestCounter(logger *zap.Logger) {
	r.TimeTicker = time.NewTicker(5000 * time.Millisecond)
	r.ReqCount = make(chan bool)
	logger.Info("timeTicker init | ReqCount Channel init.")
	go func() {
		for {
			select {
			case <-r.ReqCount:
				logger.Info("ReqCount sign received")
				atomic.AddInt32(&ReqCounter, 1)
			case t := <-r.TimeTicker.C:
				logger.Info("ticker clean started", zap.String("time at", t.String()))
				// 计数。并清零总计数 ，最后除以5
				if ReqCounter > 0 {
					qps := float32(ReqCounter / 5)
					logger.Info("qps value", zap.Float32("qps", qps))
					ReqCounter = 0
				}
			}
		}
	}()
	// time.Sleep(1600 * time.Millisecond)
	logger.Info("init done.")
}
